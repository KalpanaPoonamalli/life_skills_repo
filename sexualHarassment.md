# What kinds of behaviour cause sexual harassment?

## Sexual Harassment:

- Sexual harassment refers to unwelcome or inappropriate behavior of a sexual nature that occurs in various contexts, such as the workplace, educational settings, public spaces, or online platforms. It involves any form of unwanted sexual attention, advances, or comments that create a hostile or uncomfortable environment for the person experiencing it. Sexual harassment can happen to anyone regardless of their gender, age, or sexual orientation, and it is a violation of an individual's dignity and rights.

## Sexual Harassment: Behaviors and Examples:


### Unwanted Advances:

- Making unwelcome sexual comments or jokes.
- Requesting sexual favors in exchange for benefits.
- Persistent, unwanted flirting or attention.


### Verbal Harassment:

- Using sexually explicit language or innuendos.
- Making offensive sexual remarks or jokes.
- Engaging in sexually degrading or objectifying speech.


### Physical Invasions:

- Unwanted touching, groping, or hugging.
- Blocking someone's path or personal space.
- Forcing physical contact against their will.


### Visual Harassment:

- Displaying explicit or pornographic materials.
- Sending unsolicited sexual images or videos.
- Staring or leering in a sexual manner.


### Cyber Harassment:

- Sending sexually explicit messages or emails.
- Cyberbullying with sexual content or threats.
- Sharing private sexual content without consent.


### Hostile Environment:

- Creating a sexually intimidating or offensive atmosphere.
- Engaging in sexually explicit discussions publicly.
- Making sexually suggestive gestures or comments.


### Discrimination:

- Treating someone differently due to their gender or sexuality.
- Making derogatory remarks based on gender or orientation.
- Withholding opportunities or benefits based on sex.


### Coercion and Abuse of Power:

- Using authority or influence to demand sexual favors.
- Threatening negative consequences for refusing advances.
- Exploiting vulnerable individuals in exchange for sexual acts.


### Online Harassment:

- Cyberbullying with sexual content or remarks.
- Engaging in online stalking or harassment.
- Participating in sexually explicit online forums without consent.


### Sexual Comments on Appearance:

- Making inappropriate comments about someone's body or clothing.
- Catcalling or whistling at someone in public spaces.
- Using sexually suggestive compliments inappropriately.

** Sexual harassment is a violation of an individual's dignity and personal boundaries. It is essential to create a culture of respect and educate everyone about these behaviors to prevent and address sexual harassment effectively. **


# What would you do in case you face or witness any incident or repeated incidents of such behaviour?

### Stay Safe: 

- If you witness an incident of sexual harassment, prioritize safety. If you believe the situation may escalate or become dangerous, remove yourself from the immediate vicinity discreetly.


### Support the Victim: 
- If someone confides in you about experiencing sexual harassment, offer your support and believe their account. Let them know that you are there to listen and help in any way you can.


### Report the Incident: 
- If you are the victim or witness, consider reporting the incident to the appropriate authority or person in charge. In workplaces or educational institutions, there may be designated individuals or procedures for reporting harassment.


### Document the Incident: 
- If you can, write down the details of the incident, including dates, times, locations, and any specific comments or actions that occurred. Documentation can be helpful if further action is required.


### Encourage Others to Speak Up: 

- If you witness harassment, encourage others who may have seen it to come forward and report the incident. Their accounts can corroborate the victim's experience.


### Seek Professional Support: 

- Victims of sexual harassment may experience emotional distress or trauma. Encourage them to seek support from friends, family, or professional counselors who specialize in dealing with such situations.


### Know Your Rights: 

- Educate yourself and others about the laws and policies related to sexual harassment in your region or organization. Understanding your rights and the available recourse options is essential.


### Respect Confidentiality: 

- If someone shares their experience of harassment with you, respect their confidentiality unless they specifically request otherwise.


### Organize or Attend Training Sessions: 

- Advocate for or participate in workshops or training sessions on preventing sexual harassment. These initiatives can create awareness and foster a respectful environment.


### Report Repeated Incidents: 

- If you witness repeated incidents of sexual harassment, especially if they involve the same individuals, make sure they are reported to the appropriate authorities for proper investigation and action.


** Remember, sexual harassment is a serious issue that can have lasting effects on the victims' mental and emotional well-being. Taking a stand against such behavior and supporting victims is crucial in creating a safe and respectful society. **

