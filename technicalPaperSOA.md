# Service-Oriented Architecture (SOA)

## What is SOA?

- SOA stands for **Service Oriented Architecture**. 
- SOA is widely used in market which responds quickly and makes effective changes according to market situations.
- It is a method of software development that uses software components callse **Services** to create **Business Applications**.
- Each service provides a business capability.
- Service can also communicate with each other across platforms and languages.


## Benefits of Service-Oriented Architecture :- 

- Reuse services in different systems or combine several **Independent services** to perform complex tasks.
- Faster time [Saves time and costs]
- Easier to create, update and debug the small and large code.
- More adaptable to advances in technology.


## Components of Service-Oriented Architecture :-

- Service 
    + Service implementation
    + Service Contract
    + Service interface
- Service Provider
- Service consumer
- Service registry

## Communication Protocols of SOA :- 

- Simple Object Acces Protocol (SOAP)
- RESTful HTTP
- Apache Thrift
- Apache ActiceMQ
- Java Message Service (JMS)

## Architecture of SOA :- 

- Services/APIs
- Application
- Application Server 
- Managed Runtime Environment 
- Operating System 
- Hypervisor 
- Staorage 
- Network 

## ESB in Service-Oriented Architecture :- 

- ESB stands for ** Enterprise Service Bus **.
- An ESB provides communication and transformation capabilities through a reusable service interface.
- ESB as a centralized service that routes service requests to the appropriate service. It also transforms the request into a format that is acceptable for the service’s underlying platform and programing language.

## Overview of SOA :- 

- Service-Oriented Architecture (SOA) is a type of software design that makes software components reusable using service interfaces that use a common communication language over a network. 

- A service is a self-contained unit of software functionality, or set of functionalities, designed to complete a specific task such as retrieving specified information or executing an operation. It contains the code and data integrations necessary to carry out a complete, discrete business function and can be accessed remotely and interacted with or updated independently.

- In other words, SOA integrates software components that have been separately deployed and maintained and allows them to communicate and work together to form software applications across different systems.


## References:

- https://aws.amazon.com/what-is/service-oriented-architecture/#:~:text=you%20implement%20microservices%3F-,What%20is%20service%2Doriented%20architecture%3F,other%20across%20platforms%20and%20languages.

- https://www.ibm.com/topics/soa
