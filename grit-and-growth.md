# Grit and Mindset

# 1. Grit

## Question 1

### Paraphrase (summarize) the video in a few lines. Use your own words.

** Grit is defined as the combination of passion and perseverance to achieve long-term goals. The video emphasizes that talent alone is not enough for success; instead, one must cultivate grit to overcome challenges, maintain motivation, and continue striving despite failures. **


## Question 2

### What are your key takeaways from the video to take action on?

 The key takeaways from the video to take action on are:

- Understand that talent is not the sole determinant of success; developing grit is essential for achieving long-term goals.

- Embrace failures as learning opportunities and use them to fuel determination and perseverance.

- Cultivate passion for your pursuits, as it will provide the intrinsic motivation needed to sustain effort and dedication.

- Set specific, challenging, and realistic goals to stay focused and committed on your journey.

- Develop a growth mindset and believe that effort and practice can lead to improvement and success.

- Seek support from mentors, coaches, or a community to provide guidance, feedback, and encouragement during tough times.

- Celebrate small victories along the way to boost morale and maintain enthusiasm for your goals.



# 2. Introduction to Growth Mindset

## Question 3

### Paraphrase (summarize) the video in a few lines in your own words.

 ** It explains that people with a growth mindset believe their abilities and intelligence can be developed through effort, learning, and persistence. In contrast, individuals with a fixed mindset believe their qualities are innate and unchangeable. The video emphasizes the importance of adopting a growth mindset to overcome challenges, embrace learning opportunities, and reach one's full potential. **



## Question 4

### What are your key takeaways from the video to take action on?

### Embrace a growth mindset: 
- Believe that your abilities can be developed through dedication, practice, and learning.

### Emphasize effort and perseverance: 
- Understand that hard work and determination are crucial for personal development and achievement.

### View failures as learning opportunities: 
- Instead of fearing failure, see it as a chance to learn and grow.

### Replace self-limiting beliefs: 
- Challenge thoughts that hold you back and replace them with positive, growth-oriented beliefs.

### Seek challenges: 
- Embrace new and difficult tasks to expand your skills and knowledge.

### Learn from criticism: 
- Rather than being defensive, use feedback as a way to improve and develop further.

### Be inspired by others' success: 
- Instead of feeling envious, let the success of others motivate you to strive for your goals.
    
### Cultivate a love for learning: 
- Approach new information and skills with curiosity and excitement to foster continuous growth.


# 3. Understanding Internal Locus of Control

## Question 5

### What is the Internal Locus of Control? What is the key point in the video?

** The Internal Locus of Control is a psychological concept that refers to an individual's belief in their ability to control the events and outcomes in their life. People with an internal locus of control believe that their actions and decisions significantly influence the results they achieve, while those with an external locus of control tend to attribute their successes or failures to external factors such as luck, fate, or other people.

The key point in the video "The Locus Rule" is likely to explain the importance of having an internal locus of control for staying motivated. By understanding that they have control over their choices and actions, individuals can take ownership of their goals and aspirations, leading to increased motivation and perseverance in the face of challenges. The video likely explores strategies to cultivate an internal locus of control and highlights how adopting this mindset can positively impact one's motivation and overall life outlook. **


# 4. How to build a Growth Mindset

## Question 6

### Paraphrase (summarize) the video in a few lines in your own words.


** The process of building a growth mindset is explained. A growth mindset is about believing that one's abilities and intelligence can be developed through effort and learning. The video highlights several strategies to cultivate a growth mindset, such as embracing challenges, persisting in the face of obstacles, viewing failures as opportunities for growth, and seeking out constructive feedback. It also emphasizes the importance of self-reflection and using positive affirmations to reinforce the growth-oriented mindset. **


# Question 7

### What are your key takeaways from the video to take action on?

The key takeaways from the video to take action on are:

### Embrace challenges: 
- Welcome difficult tasks and see them as opportunities to learn and improve.

### Persist with effort: 
- Understand that progress may take time and consistent effort is essential for growth.

### View failures as learning experiences: 
- Instead of being discouraged by failures, see them as chances to identify areas for improvement.

### Cultivate a love for learning: 
- Stay curious and open to new information and skills.

### Seek constructive feedback: 
- Be willing to receive feedback from others and use it to enhance your abilities.

### Use positive affirmations: 
- Encourage yourself with positive self-talk to reinforce a growth-oriented mindset.

### Celebrate growth and effort: 
- Recognize and celebrate your progress and hard work, not just the end results.

### Avoid comparing yourself to others: 
- Focus on your own journey and improvement rather than comparing yourself to others.

### Surround yourself with growth-minded individuals: 
- Engage with people who inspire and support your growth.

### Keep a growth journal: 
- Reflect on your experiences, challenges, and lessons learned to track your growth over time.


# 5. Mindset - A MountBlue Warrior Reference Manual

## Question 8

### What are one or more points that you want to take action on from the manual?

### Take Responsibility for Learning: 
- Embrace the mindset that you are 100 percent responsible for your learning and growth. Be proactive in seeking knowledge and understanding.

### Embrace Challenges and Persevere: 
- Commit to staying with a problem until you solve it. Don't give up easily and see challenges as opportunities to learn and improve.

### Utilize Learning Tools: 
- Prioritize understanding the concepts before writing code. Make use of documentation, Google, Stack Overflow, GitHub, and the internet as resources to aid in your learning process.

### Take Ownership of Projects: 
- When working on projects, take full responsibility for their execution, delivery, and functionality.

### Embrace Discomfort and Errors: 
- Understand that discomfort and errors are natural parts of the learning process. Embrace confusion as a sign of more understanding to be achieved, discomfort as an opportunity to make efforts to understand, and errors as learning experiences to fix and improve your code.

### Stay Enthusiastic and Confident: 
- Maintain a positive and enthusiastic attitude when facing challenges or meeting people. Stand tall, sit straight, and wear confidence in your body language.

### Focus on Mastery: 
- Aim for mastery in your skills and knowledge. Strive to be so proficient that you can solve problems calmly even at 3 AM if needed.

### Complete Code with Quality: 
- Follow a checklist to ensure your code is complete and well-structured, including making it work, readable, modular, and efficient.

### Follow Problem-Solving Steps: 
- When approaching problems, relax, focus on understanding the problem, utilize available resources, code, and repeat the process as needed.